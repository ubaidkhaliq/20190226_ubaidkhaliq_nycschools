package com.app

import com.app.retrofit.WebAPI
import com.app.retrofit.WebInterface
import com.google.gson.JsonObject
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.ArgumentCaptor
import org.mockito.Mockito

import org.mockito.Mockito.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AppUnitTest {

    // Any initialisation to be done
    @Before fun beforeTestRun(){

    }

    // Test
    @Test fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
        assert(true)

        val webAPI : WebAPI = mock(WebAPI::class.java) // Mocking WebAPI class
        `when`(webAPI.getSchools()).then{  JsonObject()}

    }

    // Any cleaning up to do after test run
    @After fun afterTestRun(){

    }
}
