package com.app.retrofit

import com.google.gson.GsonBuilder
import com.app.mvp.model.SchoolList
import com.app.mvp.model.SchoolResult

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import io.reactivex.Single
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory


// Class deals with web requests for school and school details
class WebAPI {

    private var schoolAPI: WebInterface? = null
    companion object {
        private val BASE_URL = "https://data.cityofnewyork.us/resource/"
    }

    fun start() {
        val gson = GsonBuilder().run {
            setLenient()
            create()
        }

        val retrofit = Retrofit.Builder().run {
            baseUrl(BASE_URL)
            addConverterFactory(GsonConverterFactory.create(gson)) // use gson to convert
            addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // To use RxJava
            build()
        }

        schoolAPI = retrofit.create(WebInterface::class.java)
    }

    // API METHOD get list of schools using RxJava
    fun getSchools(): Single<List<SchoolList.School>> {
        return schoolAPI!!.loadSchools()
    }

    // API METHOD get sat results for school based on dbm code
    fun getSatResults(dbm: String, result: WebInterface.Result?) {

        val call = schoolAPI!!.getSchoolSat(dbm)
        call.enqueue(object : Callback<List<SchoolResult>> {
            override fun onFailure(call: Call<List<SchoolResult>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<List<SchoolResult>>, response: Response<List<SchoolResult>>) {
                if (response.isSuccessful) {
                    val list = response.body()
                    result?.onSuccess(if (list!!.size > 0) list[0] else null)
                } else {
                    println(response.errorBody())
                    result?.onError()
                }
            }
        })
    }


}

