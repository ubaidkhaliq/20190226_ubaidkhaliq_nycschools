package com.app.retrofit


import com.app.mvp.model.SchoolList
import com.app.mvp.model.SchoolResult

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import io.reactivex.Single

interface WebInterface {

    interface Result {
        fun onSuccess(result: Any?)
        fun onError()
    }

    // Get school list
    @GET("s3k6-pzi2.json")
    fun loadSchools(): Single<List<SchoolList.School>>


    // Get detail for a selected school
    @GET("f9bf-2cp4.json")
    fun getSchoolSat(@Query("DBN") dbn: String): Call<List<SchoolResult>>
}

