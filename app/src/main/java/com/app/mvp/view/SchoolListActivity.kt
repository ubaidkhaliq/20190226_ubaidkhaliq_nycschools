package com.app.mvp.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.app.R
import com.app.dagger.DIComponent
import com.app.mvp.model.One
import com.app.mvp.model.Two
import com.app.dagger.DaggerDIComponent
import com.app.mvp.model.SchoolList
import com.app.retrofit.WebAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.activity_school_list.*
import kotlinx.android.synthetic.main.school_list_content.view.*
import kotlinx.android.synthetic.main.school_list.*
import javax.inject.Inject
import javax.inject.Named

class SchoolListActivity : AppCompatActivity() {

    private lateinit var component: DIComponent

    @Inject lateinit var one : One
    @Inject @field:Named("Two")  lateinit var two : Two

    /** Whether or not the activity is in two-pane mode, i.e. running on a tablet */
    private var twoPane: Boolean = false

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_list)

        component = DaggerDIComponent.create()
        component.inject(this)
        Log.d("Template", one.a)
        Log.d("Template", two.b)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (school_detail_container != null) { twoPane = true }

        Toast.makeText(applicationContext, "loading schools please wait", Toast.LENGTH_LONG).show()

        val webAPI = WebAPI()  // Class to deal with web requests
        webAPI.start()

        webAPI.getSchools()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                { result -> kotlin.run{
                    SchoolList.list = result
                    setupRecyclerView(school_list)
                } },
                { error ->
                    Toast.makeText(applicationContext, "error loading schools ${error.localizedMessage}", Toast.LENGTH_SHORT).show()
                }
        )
    }

    // setup list view to show loaded schools
    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = SimpleItemRecyclerViewAdapter(this, SchoolList.list, twoPane)
    }

    class SimpleItemRecyclerViewAdapter(private val parentActivity: SchoolListActivity,
                                        private val values: List<SchoolList.School>,
                                        private val twoPane: Boolean) : RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val onClickListener: View.OnClickListener

        init {
            onClickListener = View.OnClickListener { v ->
                val item = v.tag as SchoolList.School
                if (twoPane) {
                    val fragment = SchoolDetailFragment().apply {
                        arguments = Bundle().apply {
                            putString(SchoolDetailFragment.ARG_ITEM_ID, item.dbn)
                        }
                    }
                    parentActivity.supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.school_detail_container, fragment)
                            .commit()
                } else {
                    val intent = Intent(v.context, SchoolDetailActivity::class.java).apply {
                        putExtra(SchoolDetailFragment.ARG_ITEM_ID, item.dbn)
                    }
                    v.context.startActivity(intent)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.school_list_content, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = values[position]
            holder.idView.text = item.dbn
            holder.contentView.text = item.school_name

            with(holder.itemView) {
                tag = item
                setOnClickListener(onClickListener)
            }
        }

        override fun getItemCount() = values.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val idView: TextView = view.id_text
            val contentView: TextView = view.content
        }
    }
}
