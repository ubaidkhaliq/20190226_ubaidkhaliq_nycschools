package com.app.mvp.view

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.app.R
import com.app.databinding.SchoolDetailBinding
import com.app.mvp.model.SchoolResult
import com.app.retrofit.WebAPI
import com.app.retrofit.WebInterface
import kotlinx.android.synthetic.main.activity_school_detail.*
import kotlinx.android.synthetic.main.school_detail.view.*
import javax.inject.Inject




/**
 * A fragment representing a single SchoolResult detail screen.
 * This fragment is either contained in a [SchoolListActivity]
 * in two-pane mode (on tablets) or a [SchoolDetailActivity]
 * on handsets.
 */
class SchoolDetailFragment : Fragment() {

    companion object {
        const val ARG_ITEM_ID = "item_id" // argument used to pass school id to fragment
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var rootView : View
    private lateinit var viewModel: SchoolResult
    private lateinit var binding: SchoolDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SchoolResult::class.java)
        arguments?.let {

            if (it.containsKey(ARG_ITEM_ID)) {

                val webAPI = WebAPI()
                webAPI.start()

                val dab = it.getString(ARG_ITEM_ID)
                webAPI.getSatResults(dab, object: WebInterface.Result{
                    override fun onSuccess(result: Any?) {
                        if(result!= null){
                            val satResult = result as SchoolResult
                            activity?.toolbar_layout?.title = satResult.schoolName
                            rootView.num_of_sat_test_takers.text = satResult.testTakers
                            rootView.sat_critical_reading_avg_score.text = satResult.readingAvgScore
                            rootView.sat_math_avg_score.text = satResult.mathAvgScore
                            rootView.sat_writing_avg_score.text = satResult.writingAvgScore
                        } else {
                            Toast.makeText(activity?.applicationContext, "results not available for school", Toast.LENGTH_SHORT ).show()
                        }
                    }

                    override fun onError() {
                        Toast.makeText(activity?.applicationContext, "error loading school results", Toast.LENGTH_SHORT ).show()
                    }
                })

            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = DataBindingUtil.inflate<SchoolDetailBinding>(inflater, R.layout.school_detail, container, false).also{
            binding = it
            it.schoolResult = viewModel
            it.lifecycleOwner = this
        }.root

        return rootView
    }


}
