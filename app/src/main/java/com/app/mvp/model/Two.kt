package com.app.mvp.model

// Dagger injection defined in DIModule class in dagger package
class Two (val b: String){

    @Override override fun toString(): String {
        return "Two.b = $b"
    }
}