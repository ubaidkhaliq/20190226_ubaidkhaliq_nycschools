package com.app.mvp.model

import android.arch.lifecycle.ViewModel
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

class SchoolResult @Inject constructor() : ViewModel(){
    @SerializedName("num_of_sat_test_takers")
    var testTakers: String? = null

    @SerializedName("sat_critical_reading_avg_score")
    var readingAvgScore: String? = null

    @SerializedName("sat_math_avg_score")
    var mathAvgScore: String? = null

    @SerializedName("sat_writing_avg_score")
    var writingAvgScore: String? = null

    @SerializedName("school_name")
    var schoolName: String? = null


}