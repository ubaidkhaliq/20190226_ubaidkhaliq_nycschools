package com.app.mvp.model

import javax.inject.Inject

class One( val a : String) {

    //  Dagger will call this constructor whenever an object of this class is created with @Inject var one: One
    @Inject constructor():this("injected from class"){}

    @Override
    override fun toString(): String { return "One.a = $a " }
}