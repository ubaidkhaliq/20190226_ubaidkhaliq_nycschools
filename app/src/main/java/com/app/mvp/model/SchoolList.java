package com.app.mvp.model;

import java.util.ArrayList;
import java.util.List;

public class SchoolList {

    public static List<School> list = new ArrayList<>();

    static public class School{
        public String school_name;
        public String dbn;
    }

}
