package com.app.dagger

import com.app.mvp.model.Two
import dagger.Module
import dagger.Provides
import javax.inject.Named

// Dependency injection module
@Module
class DIModule {

    // To use ->  @Inject lateinit var two : Two
    @Provides
    fun initTwo(): Two {
        return Two("two dagger")
    }

    // To use -> @Inject  @field:Named("Two") lateinit var two : Two
    @Provides
    @Named("Two")
    fun initTwoOtherWay(): Two {
        return Two("two the other way dagger")
    }
}