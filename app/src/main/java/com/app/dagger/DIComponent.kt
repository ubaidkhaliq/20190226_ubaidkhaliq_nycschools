package com.app.dagger

import com.app.mvp.view.SchoolListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DIModule::class])
interface DIComponent{

    fun inject(act : SchoolListActivity)
}